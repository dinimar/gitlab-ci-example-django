from datetime import datetime

from django.http import HttpResponse
from django.template import loader

from events.models import ResultEvent
# from events.models import ResultEvent
from . import helper


def live_bets(request):
    # events = ResultEvent.objects.all()
    events = ResultEvent.objects.filter(state="live").order_by("-dc")
    # events = events.filter(state_)
    # events = events.order_by("-dc")

    if (len(events) > 100):
        events = events[:100]

    helper.process_time_attrib(events)

    template = loader.get_template("bet_pages/events.html")
    context = {"events": events, "page_type": "live"}

    return HttpResponse(template.render(context, request))


def log_bets(request):
    events = ResultEvent.objects.all().filter(state="log").filter(dc__day=datetime.now().day).order_by("-dc")

    helper.process_time_attrib(events)

    template = loader.get_template("bet_pages/events.html")
    context = {"events": events, "page_type": "log"}

    return HttpResponse(template.render(context, request))
