from django.urls import path

from . import views

urlpatterns = [
    path('', views.live_bets, name="live_bets"),
    path("logs/", views.log_bets, name="log_bets")
]