from django.test import Client
from django.test import TestCase
from django.urls import reverse

from .views import live_bets, log_bets


class IndexTest(TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_live_bets(self):
        # Issue a GET request.
        response = self.client.get(reverse("live_bets"))

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        # Check view
        self.assertEquals(response.resolver_match.func, live_bets)

    def test_log_bets(self):
        # Issue a GET request.
        response = self.client.get(reverse("log_bets"))

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        # Check view
        self.assertEquals(response.resolver_match.func, log_bets)
