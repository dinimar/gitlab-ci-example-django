import pytz


def process_time_attrib(events):
    # Process timestamp for views
    utc3_tz = pytz.timezone("Europe/Moscow")  # Moscow timezone
    for ev in events:
        ev.dc = ev.dc.astimezone(utc3_tz)  # change timezone
        ev.time_str = ev.dc.strftime("%H:%M:%S")
        ev.time_atr = str(ev.dc)
