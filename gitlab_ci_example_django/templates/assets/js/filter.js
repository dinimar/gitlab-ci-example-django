var stStart = 0;
var stStop = 10;
var blStart = 0;
var blStop = 10;
var oddsStart = 1.01;
var oddsStop = 10;
var sportsArr = [];

function getCheckedSportsArr() {
    sports = [];
    checked_els = $("div input[class='form-check-input']:checked")
        .each(function (i) {
            sports.push($(this).val());
        });

    return sports;
}

function isContainChSports(sp_name) {
    // flag = false;
    for (var i in sportsArr) {
        var parsed_sp = sp_name.substr(0, sp_name.indexOf("."));
        if (parsed_sp == sportsArr[i]) {
            return true;
        }
    }

    return false;
}

function isInRange(start, val, stop) {
    return (start <= parseFloat(val) && parseFloat(val) <= stop);
}

$(document).ready(
    // check all checkboxes
    function () {
        $("div input[class='form-check-input']").prop("checked", true);
    }
);

$(document).ready(
    function () {
        document.getElementById("filter-form").addEventListener('click', function () {
            stStart = $("#st-start").val();
            stStop = $("#st-stop").val();
            blStart = $("#bl-start").val();
            blStop = $("#bl-stop").val();
            oddsStart = $("#odds-start").val();
            oddsStop = $("#odds-stop").val();
            sportsArr = getCheckedSportsArr();

            // // Show all trs
            // $("tbody tr").each(function (i) {
            //     $(this).show();
            // });

            // Hide all trs
            $("tbody tr").each(function (i) {
                $(this).hide();
            });


            if (sportsArr.length != 0) {
                // Show checked sports
                $("tbody tr").each(function (i) {
                    var sp_name = $(this).find("td.sp-name").text();
                    if (isContainChSports(sp_name)) {
                        $(this).show();
                    }
                });
            }

            // Hide within events with checked kind of sports
            $("tbody tr[style!='display: none;']").each(function (i) {
                var st = $(this).find("td.st").text();
                var bl = $(this).find("td.bl").text();
                var odds = $(this).find("td.odds").text();

                // By ST, BL, ODDS
                if (isInRange(stStart, st, stStop) &&
                    isInRange(blStart, bl, blStop) &&
                    isInRange(oddsStart, odds, oddsStop)
                ) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
                // // By BL
                // if (isInRange(blStart, bl, blStop)
                // // !isInRange(blStart, bl, blStop) &&
                // // !isInRange(oddsStart, odds, oddsStop)
                // ) {
                //     $(this).show();
                // }
                // // By odds
                // if (isInRange(oddsStart, odds, oddsStop)
                // // !isInRange(blStart, bl, blStop) &&
                // // !isInRange(oddsStart, odds, oddsStop)
                // ) {
                //     $(this).show();
                // }
            });
        });
    }
);