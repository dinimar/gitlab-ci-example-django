

            function createCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            createCookie(name, "", -1);
        }
        var sports = [];
        var lang = "en";
        function fSport() {
            for (var i = 0; i < $('.f-sport').length; i++) {
                var htm = '';
                if ($('.f-sport').eq(i).prop('checked') == false)
                    htm += '&' + $('.f-sport').eq(i).attr('id') + '=1';
            }
            return htm;
        }
        function matchList() {
            var ml = $('.top2').length;
            var htm = '';
            for (var i = 0; i < ml; i++) {

                if ($('.top2').eq(i).is(':visible'))
                    htm += '<p data-scroll="' + $('.top2').eq(i).attr('id') + '">' + $('.top2').eq(i).find('h4').text() + '</p>';
            }
            $('#matchlist').html(htm);
            $('p[data-scroll]').click(function () {
                var body = $("html, body");
                body.stop().animate({ scrollTop: $('#' + $(this).attr('data-scroll')).offset().top - 50 }, '500', 'swing', function () {
                    //     alert("Finished animating");
                });

            });
        }
        function clearTable() {
            return;
            //   console.log('temp=' + $('.top2').length);

            for (var i = 0; i < $('.top2').length; i++) {
                var temp = $('.top2').eq(i);
                temp.show();
                var temp2 = temp.children('.top3');
                var count3 = 0;
                //    console.log('temp2=' + temp2.length);
                for (var j = 0; j < temp2.length; j++) {
                    temp2.eq(j).show();
                    var temp3 = temp2.eq(j).find('.top4');
                    //   console.log('temp3=' + temp3.length);
                    var count = 0;
                    for (var k = 0; k < temp3.length; k++) {
                        if (temp3.eq(k).is(':visible'))
                            count++;
                    }
                    //     console.log('count=' + count);
                    if (count == 0) { temp.find('.top3').eq(j).hide(); }
                    if (temp.find('.top3').eq(j).is(':visible'))
                        count3++;

                }
                if (count3 == 0) temp.hide();

            }
            for (var i = 0; i < $('.f-sport').length; i++) {
                if ($('.f-sport').eq(i).prop('checked') == false)
                    //      htm += '&' + $('.f-sport').eq(i).attr('id') + '=1';
                    $('.f' + $('.f-sport').eq(i).attr("id")).hide();// else
                //  $('.f' + $('.f-sport').eq(i).attr("id")).show();
            }

        }
        function fPerc() {
            try {
                var htm = '';
                if ((parseInt($('#minperc').val()) >= 0) && (parseInt($('#minperc').val()) <= 100))
                    htm += '&minp=' + $('#minperc').val();
                if ((parseInt($('#maxperc').val()) >= 0) && (parseInt($('#maxperc').val()) <= 100))
                    htm += '&maxp=' + $('#maxperc').val();
                if (parseInt($('#minperc').val()) > parseInt($('#maxperc').val())) return '';
                return htm;
            }
            catch (ex) { return ''; }

        }
        function fVol() {
            try {
                var htm = '';
                if ((parseInt($('#minvol').val()) >= 0) && (parseInt($('#minvol').val()) <= 10000000))
                    htm += '&minv=' + $('#minvol').val();
                if ((parseInt($('#maxvol').val()) >= 0) && (parseInt($('#maxvol').val()) <= 10000000))
                    htm += '&maxv=' + $('#maxvol').val();
                if (parseInt($('#minvol').val()) > parseInt($('#maxvol').val())) return '';
                return htm;
            }
            catch (ex) { return ''; }

        }
        function fSportt() {
            for (var i = 0; i < $('.f-sport').length; i++) {
                var t = $('.f-sport').eq(i);
                if ($(t).prop('checked') == false) {
                    $('.f' + $(t).attr("id")).hide();
                    createCookie('ff' + $(t).attr("id"), '0', 100);

                } else {
                    $('.f' + $(t).attr("id")).show();
                    eraseCookie('ff' + $(t).attr("id"));

                }
            }
        }
        $('.f-sport').change(function (e) {

            fSportt();
            caclHide();
            clearTable();
            matchList();
        })
        var LASTEVENT='';
        function caclHide()
        {
            try {
                var maxst = parseInt($('#maxst').val());
                var minst = parseInt($('#minst').val());
                var maxbl = parseInt($('#maxbl').val());
                var minbl = parseInt($('#minbl').val());
                var mino = parseFloat($('#minodd').val().replace(',', '.'));
                var maxo = parseFloat($('#maxodd').val().replace(',', '.'));
                if ((maxo > 1000) || (maxo < 1.01)) return;
                if ((mino > 1000) || (mino < 1.01)) return;
                if ((maxst > 50) || (maxst < 0)) return;
                if ((minst > 50) || (minst < 0)) return;
                if ((maxbl > 10) || (maxbl < 0)) return;
                if ((minbl > 10) || (minbl < 0)) return;
                if (minst > maxst) return;
                if (minbl > maxbl) return;

                //   console.log('1');
                createCookie('fmaxst', maxst.toString(), 100);
                createCookie('fminst', minst.toString(), 100);
                createCookie('fmaxbl', maxbl.toString(), 100);
                createCookie('fminbl', minbl.toString(), 100);
                createCookie('fmino', mino.toString().replace(',','.'), 100);
                createCookie('fmaxo', maxo.toString().replace(',', '.'), 100);
                // console.log('2');
                var sound=0;
                for (var i = 0; i < $('.top4').length; i++) {

                    var datast = parseFloat($('.top4').eq(i).attr('data-st').replace(',', '.'));
                    var databl = parseInt($('.top4').eq(i).attr('data-bl'));
                    var dataod = parseFloat($('.top4').eq(i).attr('data-odd'));
                    if ((datast > maxst) || (datast < minst) || (dataod > maxo) || (dataod < mino) || (databl > maxbl) || (databl < minbl)) $('.top4').eq(i).hide();
                    if ((LASTEVENT!= $('.top4').eq(i).attr('data-curr'))&&($('.top4').eq(i).is(':visible'))&&(sound==0))
                    {
                        var sound1 = $(".player_audio")[0];

                  //     sound1.load();
                     //   sound1.play();
               
                        LASTEVENT = $('.top4').eq(i).attr('data-curr');
                        sound=1;
                    }
                    //       else $('.top4').eq(i).show();
                }
               // clearTable();
                matchList();
               
            } catch (Ex) { console.log(Ex); }
        }
        $('#reset').click(function () {
            $('.f-sport').prop('checked', true);
            for (var i = 0; i < $('.f-sport').length; i++) {
                var t = $('.f-sport').eq(i);
                eraseCookie('ff' + $(t).attr("id"));
            }
            $('#maxst').val(50);
            $('#minst').val(0);
            $('#maxbl').val(10);
            $('#minbl').val(0);
            $('#minodd').val('1.01');
            $('#maxodd').val(1000);
            caclHide();
        });

        $('.stw').keyup(function () {

            caclHide();
        });
        $('.oddw').keyup(function () {

            caclHide();
        });
        $('.blw').keyup(function () {

            caclHide();
        });
        $('.stw').click(function () {


            caclHide();
        });
        $('.oddw').click(function () {

            caclHide();
        });
        $('.blw').click(function () {

            caclHide();
        });
        var s1 = "Sport", s2 = "League", s3 = "Competition", s4 = "Period",
                    s5 = "Bet", s6 = "Odds", s7 = "Current Odd", s8 = "ST", s9 = "Last Update", s10 = "hidden"
                    , s11 = "BL";
        if (lang == "ru")
        {
            s1 = "Спорт"; s2 = "Лига"; s3 = "Соревнование"; s4 = "Период";
            s5 = "Ставка"; s6 = "Коэф."; s7 = "Текущий коэф."; s8 = "ST"; s9 = "Дата прогруза";
            s10 = "скрыто"; s11 = "BL";
        }
        function detectHide(a, b, c, d) {
            return '';
        }
        function detectHide2(a, b, c, d,e,f) {
            return '';
        }
        function ToolTipHtml( curr,  odd,  date,  sub,  lang)
        {
            //  try
            // {
            //   console.log(curr, odd, date, sub, lang);
            // return "";
            if (odd == "") return "";
            var h = odd.replace('.', ',').substring(1).split('*');
            var i = date.substring(1).split('*');
            var globhtml = "";
            var tempOdd = 0;
            var scl = "";
            var l = h.length;
            if (!sub && h.length > 2) l = 2;// :l= h.Length);
            for (var t = 0; t < l; t++)
            {
                scl = "";
                if (tempOdd != 0)
                {
                    if (tempOdd > h[t])
                    {
                        scl = " class='downr' ";
                    }
                    if (tempOdd < h[t])
                    {
                        scl = " class='upr' ";
                    }

                }
                tempOdd = h[t];
                dt = i[t];
                globhtml += "<span>" + dt + " <b " + scl + " >" + h[t] + "</b></span><br/>";
            }
            var err = "To show all odds buy subscription";
            if (lang == "ru") err = "Купите подписку для отображения всех коэффициентов";
            if (!sub) return (globhtml + "<b>" + err + "</b>");
            return globhtml;
            // }
            //  catch (ex) { return ""; }
        }
        function ParseLay( s,  n,  hidden)
        {
            if (s == "") return "";
            try
            {
                var t = s.split(';');

                var html = "";
                var tempmax = 0;
                for (var i = 0; i < t.length - 1; i++)
                {
                    var h = t[i].split('*');
                    if ( h[1].substring(0, h[1].indexOf(','))> tempmax) {
                        html = "" + h[0] + " : <b>" + (n == true ? h[1].substring(0, h[1].indexOf(',')) : hidden) + " $</b>";
                        tempmax = h[1].substring(0, h[1].indexOf(','));
                    }
                }
                return html;
            }
            catch (ex) { return hidden; }

        }
        function addArr(lay,odd)
        {
            lay = lay.substr(1, lay.indexOf('*', 1));
            lay= parseFloat(lay.replace(',','.'));
            odd= parseFloat(odd.replace(',','.'));

            var img = "";
            if (lay < odd) return " <img src='/Img/aru.png' /> ";
            if (lay > odd) return " <img src='/Img/ard.png' /> ";
            return "";
        }
        function dynamicSort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        function getScore(wwa)
        {//try{
            wwa = wwa.toLowerCase();

            var home = wwa.substr(0, wwa.indexOf(' v ')).trim();
            var away = wwa.substr(wwa.indexOf(' v ') + 3).trim();
            homel=home.length;
            awayl = away.length;
            if (home.indexOf(' ')>-1)
                home = home.substr(home.indexOf(' ') + 1);
            if (away.indexOf(' ') > -1)
                away = away.substr(away.indexOf(' ') + 1);
            if (homel>5) home = home.substr(0,4);
            if (awayl>5) away = away.substr(0,4);

            var html = '';
            var tempSn = '', tempL = '';
            $.each(sports, function (key, game) {
                try{
                    var gl = game.L;
                    var gs = game.SN;
                    // console.log(gl + ':::' + gl.indexOf('Alternative'));
                    if (gl.indexOf('Alternative') == -1) {

                        var g1 = game.O1.toLowerCase().trim(); var g2 = game.O2.toLowerCase().trim();
                        //  g1l=g1.length;
                        //   g2l=g2.length;
                        //  if (g1l>5) g1 = g1.substr(0,4);
                        //    if (g2l>5) g2 = g2.substr(0,4);
                        if ((g1.indexOf(home) > -1) && (g2.indexOf(away) >-1))
                        {
                            html+= "<b>"+ (game.SC.FS.S1 == null ? "0" : game.SC.FS.S1) + ":" + (game.SC.FS.S2 == null ? "0" : game.SC.FS.S2);
                            var temphtml = "("; var isbeg = true;
                            $.each(game.SC.PS, function (k, g) {
                                if (!isbeg) {
                                    temphtml += "  ";
                                }
                                temphtml += (g.Value.S1 == null ? "0" : g.Value.S1) +
                                 ":" + (g.Value.S2 == null ? "0" : g.Value.S2);
                                isbeg = false;
                            });
                            temphtml += ")";
                            if (temphtml != '()') html += temphtml + " ";
                            html+="</b> ";
                            if (game.SC.TS != null) {
                                var mins = Math.floor(game.SC.TS / 60);
                                var secs = game.SC.TS - 60 * mins;
                                html += " " + mins+" min";
                            }
                            return false;
                            // return html;
                        }
                        if ((g1.indexOf(away) > -1) && (g2.indexOf(home) > -1))
                        {
                            html+= "<b>"+   (game.SC.FS.S2 == null ? "0" : game.SC.FS.S2)+ ":" +(game.SC.FS.S1 == null ? "0" : game.SC.FS.S1);
                            var temphtml = "("; var isbeg = true;
                            $.each(game.SC.PS, function (k, g) {
                                if (!isbeg) {
                                    temphtml += "  ";
                                }
                                temphtml += + (g.Value.S2 == null ? "0" : g.Value.S2)
                                ":" + (g.Value.S1 == null ? "0" : g.Value.S1);
                                isbeg = false;
                            });
                            temphtml += ")";
                            if (temphtml != '()') html += temphtml + " ";
                            html+="</b> ";
                            if (game.SC.TS != null) {
                                var mins = Math.floor(game.SC.TS / 60);
                                var secs = game.SC.TS - 60 * mins;
                                html += " " + mins+" min";
                            }
                            return false;
                            // return html;
                        }

                    }
                } catch (ex) { return html; console.log(ex.toString()); }
            });
            return html;

            //  } catch (ex) { console.log(ex.toString());}
        }
        
        function AfterAjax(data)
        {

            events = data.events;
            var l = data.events.length;
            
            var html = "", type = "", eventId = "", marketId = "", sectionId = "", minp = '', maxp = '', minv = '', maxv = '';
            html += "<table class='rT table-responsive tablesorter' id='tableSort'><thead><tr><th width='5%'>" + s1 + "</th><th width='25%'>" + s2 + "</th><th  width='25%'>" + s3 + "</th><th  width='10%'>" + s4 + "</th><th  width='10%'>" + s5 + "</th><th  width='10%'>" + s6 + "</th><th  width='5%'>" + s8 + "</th><th  width='5%'>" + s11 + "</th><th  width='5%'>" + s9 + "</th></tr></thead>";

            
            for (var i = 0; i < l; i++) {
                var CURREVENT = events[i].S1+events[i].S2+events[i].S3+events[i].S4+events[i].BET;
                CURREVENT = CURREVENT.toLowerCase();
                html += "<tr class='top4 ff"+events[i].S1.substr(0,3)+" " + (events[i].BL > 0 ? (events[i].BL > 1 ? "level2" : "level1") : "") + "' id='si" + events[i].ID + "' data-curr='"+CURREVENT+"' data-bl='" + events[i].BL+ "' data-st='" + events[i].ST + "'  data-odd='" + events[i].K2.replace(',','.') + "' ><td>" + events[i].S1 + "</td><td>" + events[i].S2 + "</td><td>" + events[i].S3 + "</td><td>" + events[i].S4 + "</td><td>" + events[i].BET + "</td><td><a hre='#' title=\"" +ToolTipHtml(events[i].K2, events[i].OtherPrice, events[i].OtherDate, 1, lang)   + "\"  data-html='true' data-placement='left'  rel='tooltip'><b>" + events[i].K2 + "</b></a></td><td>" + events[i].ST + "</td><td>" + events[i].BL + "</td><td>" + events[i].DC + "</td></tr>";
                 
            }
            html += "</table>";
            $('#divlost').html(html);
         //   matchList();


            fSportt();
            caclHide();
            try { $('a[rel="tooltip"]').tooltip(); } catch (ex) { }

        }
        $(document).ready(function () {
            var userAuthorized = false;
            if (userAuthorized) {
                if (readCookie('fmaxst') != null)
                    $('#maxst').val(readCookie('fmaxst'));
                if (readCookie('fminst') != null)
                    $('#minst').val(readCookie('fminst'));
                if (readCookie('fmaxbl') != null)
                    $('#maxbl').val(readCookie('fmaxbl'));
                if (readCookie('fminbl') != null)
                    $('#minbl').val(readCookie('fminbl'));
                if (readCookie('fmino') != null)
                    $('#minodd').val(readCookie('fmino'));
                // alert(readCookie('mino'));
                if (readCookie('fmaxo') != null)
                    $('#maxodd').val(readCookie('fmaxo'));

                for (var i = 0; i < $('.f-sport').length; i++) {
                    var t = $('.f-sport').eq(i);
                    if (readCookie('ff' + $(t).attr("id")) != null)
                    {
                        //console.log(readCookie('ff' + $(t).attr("id")));
                        $(t).prop('checked', false);
                    }
                } }

            $.ajax({
                url: 'FonbetLiveTest',
                dataType: 'json',
                success: function (dataa) {

                    AfterAjax(dataa);
                    //         caclHide();
                }
            });
            worker();
            //       caclHide();
            //scores();
        });
        function worker() {
            setInterval(function () {
                $.ajax({
                    url: 'FonbetLiveTest',
                    dataType: 'json',
                    success: function (dataa) {

                        AfterAjax(dataa);
                    }
                });
            }, 10000);
        }
