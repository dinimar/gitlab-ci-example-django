var api_url = 'api/fonbet/';
var lang = 'ru';
var lang_path = '/' + lang + '/';
var packetVersion = null;
var h_num_stack = [0, 1, 2, 3];
var HOST = "https://fonbet-bets-ci.herokuapp.com/";
// HOST = "http://127.0.0.1:8000/";
var API_URL = "api/events/";
var UPD_EV = "last_upd/";
var MAX_ROWS = 100;

function addTrs(ev_arr) {
    // remove rows if there are more than 100 rows
    var rows = $("tr");
    if (rows.length + ev_arr.length > MAX_ROWS) {
        var del_num = (rows.length + ev_arr.length) - MAX_ROWS;
        for (var i = 0; i < del_num; i++) {
            $("tbody tr:last").remove();
        }
    }

    // sort array
    ev_arr.sort(function (ev_a, ev_b) {
        return ev_b.dc - ev_a.dc;
    });

    for (var ev in ev_arr) {
        // remove already existing row
        // var q_str = "tr[time='"+ev_arr[ev].ev_id+"']";
        var el = $("#" + ev_arr[ev].ev_id);
        el.fadeOut();
        el.remove();

        var time_dc = new Date(Date.parse(ev_arr[ev].dc));
        var time_str = time_dc.toLocaleTimeString("ru-RU", {timeZone: "Europe/Moscow"});
        $("tbody").prepend("" +
            "<tr class='event' id='" + ev_arr[ev].ev_id + "'>" +
            "<td class='sp-name'>" + ev_arr[ev].sp_name + "</td>" +
            "<td class='ev-name'>" + ev_arr[ev].ev_name + "</td>" +
            "<td class='period'>" + ev_arr[ev].period + "</td>" +
            "<td class='bet-name'>" + ev_arr[ev].bet_name + "</td>" +
            "<td class='odds'>" + ev_arr[ev].coef + "</td>" +
            "<td class='st'>" + ev_arr[ev].st + "</td>" +
            "<td class='bl'>" + ev_arr[ev].bl + "</td>" +
            "<td class='last-update' time='" + ev_arr[ev].dc + "'>" + time_str + "</td>" +
            "</tr>");

        highlight(document.getElementById(ev_arr[ev].ev_id));
    }
}

function highlight(obj) {
    var orig = obj.style.backgroundColor;
    obj.style.backgroundColor = "#00FF83";
    setTimeout(function () {
        obj.style.backgroundColor = orig;
    }, 1000);
}

function findObjectById(collection, id) {
    for (const [key, value] of Object.entries(collection)) {
        if (value['id'] == id) return (value['team1'] + ' ' + value['team2']);
    }
}

function parseEventBlocks(sports, events, eventBlocks) {
    'use strict';

    // for (const [key, value] of Object.entries(eventBlocks)) {
    //     addTr("", findObjectById(sports, value['eventid']));
    // }
    addTrs("", findObjectById(events, eventBlocks[0]['eventId']))
}

// Get last event time from table
function getLastEventTime() {
    var cur_time = new Date();
    cur_time.setMinutes(cur_time.getMinutes() - 5);
    th_time = $("table tbody tr:first td[time]");
    console.log(th_time.attr("time"));
    time = (th_time.attr("time") == null) ? cur_time : th_time.attr("time");
    console.log(time);
    return time;
}

function updateFromLastEvent() {
    // Get new rows
    $.ajax({
        method: "POST",
        url: HOST + API_URL + UPD_EV,
        data: JSON.stringify({"timestamp": getLastEventTime()}),
        contentType: "application/json; charset=utf-8",
        // dataType: "json",
    }).done(function (data) {
        console.log(data);
        // var ev = data[0];
        // ev_arr = $.parseJSON(data);
        // data.forEach(item => ev_arr.push(JSON.parse(item)));
        // var jsonData = JSON.parse(data);
        // for (var ev in data.items) {
        //     console.log(ev);
        // }

        if (data.length != 0) {
            addTrs(data);
        }


    });

    // console.log(getLastEventTime())
    // $.getJSON(host+api_url, pidor(data));

    // function onSuccess(data) {
    //     console.log(data);
    // }
        // Get last packetVersion
    // packetVersion = data['packetVersion'];
        // Parse blocked events
    // resultEvents = parseEventBlocks(data['sports'],
    //     data['events'],
    //     data['eventBlocks']);


    // if (h_num_stack.length == 0) {
    //     console.log('List updated');
    //     h_num_stack = [0, 1, 2, 3];
    // }
        // console.log(h_num_stack);
        // pr_h_num = h_num;
        // h_num_stack.push(h_num);
        // h_num_stack.shift();
    // });
}


$(document).ready(function () {
    // init to amount of rows before ajax update
    // MAX_ROWS = $("tr").length;
    setInterval(updateFromLastEvent, 10000);
});


