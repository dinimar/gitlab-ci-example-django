from django.db import models


class ResultEvent(models.Model):
    EVENT_STATES = (
        ("live", "Log"),
        ("log", "Live")
    )
    ev_id = models.CharField(max_length=255, unique=True)
    sp_name = models.CharField(max_length=100)
    ev_name = models.CharField(max_length=50)
    period = models.CharField(max_length=15, blank=True)
    bet_name = models.CharField(max_length=20)
    coef = models.FloatField()
    st = models.IntegerField()
    bl = models.IntegerField()
    dc = models.DateTimeField()
    state = models.CharField(max_length=4, choices=EVENT_STATES,
                             default="live", blank=True)
