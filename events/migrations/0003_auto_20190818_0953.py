# Generated by Django 2.0.1 on 2019-08-18 06:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('events', '0002_auto_20190817_1206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resultevent',
            name='ch_name',
        ),
        migrations.AlterField(
            model_name='resultevent',
            name='sp_name',
            field=models.CharField(max_length=100),
        ),
    ]
