# Generated by Django 2.0.1 on 2019-08-22 06:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('events', '0008_auto_20190821_0910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resultevent',
            name='state',
            field=models.CharField(blank=True, choices=[('live', 'Log'), ('log', 'Live')], default='live',
                                   max_length=4),
        ),
    ]
