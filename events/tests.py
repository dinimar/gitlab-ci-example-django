import json
from datetime import datetime

from django.test import Client
from django.test import TestCase
from django.urls import reverse
from pytz import timezone
from rest_framework import status

from events.models import ResultEvent
from events.serializers import ResultEventSerializer


class ResultEventTestCase(TestCase):
    def setUp(self):
        self.MSC_TZ = timezone('Europe/Moscow')
        self.today_date = datetime.now(tz=self.MSC_TZ)
        yester_date = self.today_date.replace(day=self.today_date.day - 1)
        self.res_event1 = ResultEvent(
            ev_id="7243_GT",
            sp_name='Футбол Чемпионат',
            ev_name='Зенит - Москва',
            period='1-й тайм',
            bet_name='Победа 1',
            coef=1.1,
            st=1,
            bl=0,
            dc=self.today_date)
        self.res_event2 = ResultEvent(
            ev_id="7242_GT",
            sp_name='Баскетбол Чемпионат',
            ev_name='Лэйкерс - Буллс',
            period='1-й период',
            bet_name='Победа 2',
            coef=2.1,
            st=1,
            bl=0,
            dc=yester_date)
        self.res_event1.save()
        self.res_event2.save()
        # Every test needs a client.
        self.client = Client()

    def test_get_single_event_pk(self):
        response = self.client.get(
            reverse('event_details', kwargs={'pk': self.res_event1.pk}))
        test_event_id_1 = ResultEvent.objects.get(id=self.res_event1.pk)
        serializer = ResultEventSerializer(test_event_id_1)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_event_ev_id(self):
        response = self.client.get(
            reverse('event_details_ev_id', kwargs={'ev_id': self.res_event1.ev_id}))
        test_event_id_1 = ResultEvent.objects.get(ev_id=self.res_event1.ev_id)
        serializer = ResultEventSerializer(test_event_id_1)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_single_event_ev_id(self):
        put_event = self.res_event1

        obj_dict = {"ev_id": put_event.ev_id,
                    "sp_name": put_event.sp_name,
                    "ev_name": put_event.ev_name,
                    "period": put_event.period,
                    "bet_name": put_event.bet_name,
                    "coef": put_event.coef,
                    "st": put_event.st,
                    "bl": put_event.bl,
                    "dc": str(put_event.dc)}

        response = self.client.put(
            reverse('event_details_ev_id', kwargs={'ev_id': put_event.ev_id}),
            json.dumps(obj_dict),
            content_type="application/json")
        test_event_id_1 = ResultEvent.objects.get(ev_id=put_event.ev_id)
        serializer = ResultEventSerializer(test_event_id_1)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_events(self):
        event_set = ResultEvent.objects.all()
        response = self.client.get(
            reverse('event_list'))
        serializer = ResultEventSerializer(event_set, many=True)
        self.assertJSONEqual(str(response.content, encoding='utf-8'), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_single_event_ev_id(self):
        response = self.client.delete(
            reverse('event_details_ev_id', kwargs={'ev_id': self.res_event2.ev_id}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_post_single_event(self):
        event_json = {
            "ev_id": "7241_G",
            "sp_name": "Басектбол Чемпионат",
            "ev_name": "Лэйкерс - Буллс",
            "period": "1-й период",
            "bet_name": "Победа 2",
            "coef": 2.1,
            "st": 1,
            "bl": 0,
            "dc": "2012-02-21 10:28:45+0300",
            "state": "log"
        }

        response = self.client.post(
            reverse('event_list'),
            data=json.dumps(event_json),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_yester_events(self):
        response = self.client.delete(reverse('empty_yester_log_events'))
        all_events = ResultEvent.objects.all()
        today_events = ResultEvent.objects.all().filter(dc__day=self.today_date.day)

        self.assertQuerysetEqual(all_events, today_events)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_all_events(self):
        response = self.client.delete(reverse('event_list'))
        all_objects = ResultEvent.objects.all()
        self.assertEquals(0, len(all_objects))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
