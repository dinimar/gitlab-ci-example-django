from rest_framework import serializers

from events.models import ResultEvent


class ResultEventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ResultEvent
        fields = ['id', 'ev_id', 'sp_name', 'ev_name',
                  'period', 'bet_name', 'coef',
                  'st', 'bl', 'dc', 'state']
