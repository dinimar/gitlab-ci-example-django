from django.urls import path

from . import views

urlpatterns = [
    path('events/', views.res_event_list, name="event_list"),
    path('events/<int:pk>/', views.res_event_detail, name="event_details"),
    path('events/ev_id/<str:ev_id>/', views.res_event_detail_ev_id, name="event_details_ev_id"),
    path('events/last_upd/', views.res_events_by_timestamp, name="events_by_timestamp"),
    path('events/empty_log/', views.empty_yester_log_events, name="empty_yester_log_events"),
]
