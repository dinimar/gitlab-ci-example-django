from datetime import datetime

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from pytz import timezone
from rest_framework.parsers import JSONParser

from events.models import ResultEvent
from events.serializers import ResultEventSerializer


@csrf_exempt
def res_event_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        res_events = ResultEvent.objects.all()
        serializer = ResultEventSerializer(res_events, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ResultEventSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == "DELETE":
        res_events = ResultEvent.objects.all()
        res_events.delete()
        return HttpResponse(status=204)


@csrf_exempt
def res_events_by_timestamp(request):
    data = JSONParser().parse(request)
    upd_time = data["timestamp"]
    if request.method == 'POST':
        res_events = ResultEvent.objects.all().filter(dc__gt=upd_time)

        # response = {}
        # response['event_list'] = serializers.serialize("json", res_events)
        # return HttpResponse(response, content_type="application/json")

        serializer = ResultEventSerializer(res_events, many=True)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def empty_yester_log_events(request):
    if request.method == 'DELETE':
        MSC_TZ = timezone('Europe/Moscow')
        # remove all rows that not created yesterday
        res_events = ResultEvent.objects.all().filter(dc__lt=datetime.now(tz=MSC_TZ))
        res_events.delete()

        return HttpResponse(status=204)


@csrf_exempt
def res_event_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        res_event = ResultEvent.objects.get(pk=pk)
    except ResultEvent.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ResultEventSerializer(res_event)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ResultEventSerializer(res_event, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        res_event.delete()
        return HttpResponse(status=204)


@csrf_exempt
def res_event_detail_ev_id(request, ev_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        res_event = ResultEvent.objects.get(ev_id=ev_id)
    except ResultEvent.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ResultEventSerializer(res_event)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ResultEventSerializer(res_event, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        res_event.delete()
        return HttpResponse(status=204)
